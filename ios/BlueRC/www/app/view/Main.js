Ext.define('BlueRC.view.Main', {
    extend:'Ext.tab.Panel',
    xtype:'main',
    requires:[
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config:{
        tabBarPosition:'bottom',

        items:[
            {
                title: 'Prefs',
                iconCls: 'action',
                scrollable: true,
                layout: {
                    type: 'vbox',
                    align: 'middle'
                },
                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Preferences'
                    },
                    {
                        xtype: 'prefs',
                        itemId: 'prefs1',
                        style: {width: "100%"}
                    }
                ]
            },
            {
                title:'Car',
                iconCls:'home',
                layout:'vbox',
                items:[
                    {
                     docked: 'top',
                     xtype: 'titlebar',
                     title: 'Car'
                     },
                    {
                        xtype:'joystick',
                        itemId:'joystick1',
                        flex:1
                    }
                ]
            },
            {
                title:'Leds',
                iconCls:'star',
                scrollable:false,
                layout:{
                    type:'vbox', 
                    align:'middle'
                },
                items:[
                    {
                        docked:'top',
                        xtype:'titlebar',
                        title:'RGB LEDs'
                    },
                    {
                        width:345,
                        xtype:'led',
                        itemId:'led1'
                    }
                ]
            }
        ]
    }
});