Ext.define('BlueRC.view.Prefs', {
    extend: 'Ext.Container',
    xtype: 'prefs',
    currentOutput:0,

    config: {
        margin: 10,
        layout: {
            type: 'vbox',
            pack: 'center',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'button',
                iconCls: 'star',
                iconMask: true,
                text: 'Test 0',

                style: {
                    marginLeft: '5px',
                    marginRight: '5px'
                },
                handler: function () {

                    var self = this.up('prefs');

                    self.currentOutput++;

                    if (self.currentOutput > 16)
                        self.currentOutput = 0;
             

                    self.fireEvent("testUpdate", self.currentOutput);


                    this.setText("Test " + self.currentOutput);

                }
            },
            {
                xtype: 'sliderfield',
                label: 'us',
                value: 128,
                minValue: 25,
                maxValue: 230,
                listeners:
                {
                    drag:function(field, slider, thumb, e, eOpts){
                        packetBuffer = [];
                        
                        packetBuffer[0] = 6;
                        
                        for(var i = 1; i <= 8; i++)
                            packetBuffer[i] = slider.getValue();

                        BluetoothServoController.SendMessage("Fast Servo Control", packetBuffer, false);
                    }

                }
            },
            {
                xtype: 'sliderfield',
                label: 'us',
                value: 128,
                minValue: 25,
                maxValue: 230,
                listeners: {
                    drag: function (field, slider, thumb, e, eOpts) {
                        packetBuffer = [];

                        packetBuffer[0] = 38;

                        for (var i = 1; i <= 8; i++)
                            packetBuffer[i] = slider.getValue();

                        BluetoothServoController.SendMessage("Fast Servo Control", packetBuffer, false);
                    }

                }
            },
            {
                xtype:'textfield',
                itemId: 'ipField',
                label:'IP',
                value:'Not Available'
            }
            
        ]
    },
    initialize: function () {
        console.log("Prefs initialize...");
        this.callParent();
        
        var self = this;
        
        BluetoothServoController.GetIpAddress(function(ipAddress)
        {
            console.log(ipAddress);
            self.down('#ipField').setValue(ipAddress + " port 5555");
            //self.up('#ipField').setValue(ipAddress);
        });

    }
});