//<debug>
Ext.Loader.setPath({
    'Ext':'touch/src',
    'BlueRC':'app'
});
//</debug>

Ext.application({
    name:'BlueRC',

    requires:[
        'Ext.MessageBox'
    ],

    views:['Main', 'Joystick', 'Led', 'Prefs'],
    controllers:['Blue'],

    icon:{
        '57':'resources/icons/Icon.png',
        '72':'resources/icons/Icon~ipad.png',
        '114':'resources/icons/Icon@2x.png',
        '144':'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed:true,

    startupImage:{
        '320x460':'resources/startup/320x460.jpg',
        '640x920':'resources/startup/640x920.png',
        '768x1004':'resources/startup/768x1004.png',
        '748x1024':'resources/startup/748x1024.png',
        '1536x2008':'resources/startup/1536x2008.png',
        '1496x2048':'resources/startup/1496x2048.png'
    },

    launch:function () {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        BluetoothServoController = {};

        BluetoothServoController._messageBuffer = "";

        BluetoothServoController.SendMessage = function (characteristic, data, withResponse) {

            var base64Data = BluetoothServoController.Encode(data);

            if (typeof device !== "undefined") {
                cordova.exec(
                    function (message) {
                        //console.log("BluetoothServoController.SendMessage.Message:");
                        //console.log(message);
                    },
                    function (error) {
                        console.log("BluetoothServoController.SendMessage.Error:");
                        console.log(error);
                    },
                    "BluetoothServoController",
                    "sendData",
                    [ "" + characteristic, "" + base64Data, "" + withResponse]
                );
            }
            else {
                console.log("Psudo-send: " + characteristic + ", " + data + ", " + withResponse);
            }
        };

        BluetoothServoController.GetIpAddress = function (callback) {

            if (typeof device !== "undefined") {
                cordova.exec(
                    function (message) {
                        callback(message);
                    },
                    function (error) {
                        console.log("BluetoothServoController.GetIpAddress.Error:");
                        console.log(error);
                    },
                    "BluetoothServoController",
                    "getIpAddress",
                    []
                );
            }
            else {
                console.log("Psudo-send: getIpAddress");
            }
        };

        BluetoothServoController.Encode = function(data) {
            var str = "";
            for (var i = 0; i < data.length; i++)
                str += String.fromCharCode(data[i]);

            return btoa(str).split(/(.{75})/).join("\n").replace(/\n+/g, "\n").trim();
        };

        BluetoothServoController.SendServoControl = function (servoData) {
            var data = [];
            var lastByte = parseInt("ff", 16);
            
            var servoDataArray = servoData.split(":");
            
            var channel = parseInt(servoDataArray[0]);
            var startValue = parseInt(servoDataArray[1]);
            var stopValue = parseInt(servoDataArray[2]);
            
            channel = ((channel - 1) * 4) + 6;
            
            data[0] = channel;
            data[1] = startValue & lastByte;
            data[2] = (startValue>>8) & lastByte;
            data[3] = stopValue & lastByte;
            data[4] = (stopValue >> 8) & lastByte;

            BluetoothServoController.SendMessage("Channel Control", data, false);

        };
        
        BluetoothServoController.servoCache = [];
        
        BluetoothServoController.SendFastServoControl = function(servoData){
            //console.log("SendFastServoControl");
            
            var packetBuffer = [];

            var servoDataArray = servoData.split(":");

            var channel = parseInt(servoDataArray[0]);
            var value = parseInt(servoDataArray[1]);
            
            BluetoothServoController.servoCache[channel] = value;

            
            var offset = 0;

            if (channel <= 8) {
                packetBuffer[0] = 6;
            }
            else {
                offset = 8;
                packetBuffer[0] = 38;
            }
            
            for(var i = (1 + offset); i <= (8 + offset); i++)
            {
                channel = i;
                value = BluetoothServoController.servoCache[channel];
                if(value)
                {
                    packetBuffer[i - offset] = value;
                }
                else
                {
                    packetBuffer[i - offset] = 128;
                }
            }

            BluetoothServoController.SendMessage("Fast Servo Control", packetBuffer, false);
        };


        BluetoothServoController.ReceiveMessage = function (_message) {
            return;
            //this is not ready yet...
            
            var message = atob(_message);

            //LoopTest
            BluetoothServoController.SendMessage(message);

            //console.log(message);
            BluetoothServoController._messageBuffer += message;

            var index1 = BluetoothServoController._messageBuffer.indexOf('!');
            var index2 = BluetoothServoController._messageBuffer.indexOf('\r');

            if (index1 != -1 || index2 != -1) {
                var index;

                if (index1 != -1 && index2 != -1)
                    index = Math.min(index1, index2)
                else if (index1 != -1)
                    index = index1;
                else if (index2 != -1)
                    index = index2;

                message = BluetoothServoController._messageBuffer.substr(0, index);
                BluetoothServoController._messageBuffer = BluetoothServoController._messageBuffer.substr(index + 2);

                BlueRC.app.fireEvent('message', { Message:message });
            }

        };

        //this initializes the plugin...
        if (typeof device !== "undefined") {
            cordova.exec(
                BluetoothServoController.ReceiveMessage,
                function (error) {
                    console.log(error);
                },
                "BluetoothServoController",
                "onData"
            );
        }

        // Initialize the main view
        Ext.Viewport.add(Ext.create('BlueRC.view.Main'));
    },

    onUpdated:function () {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function (buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
