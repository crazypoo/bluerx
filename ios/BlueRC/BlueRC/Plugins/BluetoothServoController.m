//
//  BluetoothServoController.m
//  BlueRC
//
//  Created by Jason Peterson on 4/6/13.
//
//

#import "BluetoothServoController.h"
#import "Base64.h"

@implementation BluetoothServoController

NSTimer * thisTimer;
StpBlueRC * bluetooth;

@synthesize currentCallbackId = _currentCallbackId;
@synthesize isRegistered = _isRegistered;

- (NSString *)getIPAddress {
    
    NSString *address = @"WiFi not found...";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

-(void) asyncTest:(NSTimer *)timer
{
    NSLog(@"Obj-C asyncTest");
    
    if ([self isRegistered])
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"JS asyncTest"];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:[self currentCallbackId]];
    }
    
}

-(void) receivedData:(NSNotification *)theNotification
{
    //NSLog(@"SCP - receivedData from bluetooth");
    
    if([self isRegistered])
    {
        //NSLog(@"SCP - receivedData isRegistered");
        
        NSData *theData = [[theNotification userInfo] objectForKey:@"Data"];
        
        if(theData)
        {
            //NSLog(@"SCP - receivedData theData");
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[theData base64EncodedString]];
            [pluginResult setKeepCallbackAsBool:YES];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:[self currentCallbackId]];
        }
    }
}

-(void) initialize
{
    NSLog(@"initialize BluetoothServoController");
    bluetooth = [StpBlueRC sharedInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedData:) name:@"ReceivedData" object:nil];
    [self setIsRegistered:YES];
}

- (void) onData:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
    [pluginResult setKeepCallbackAsBool:YES];
    [self setCurrentCallbackId:command.callbackId];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    if(![self isRegistered])
        [self initialize];
    
    
    if(thisTimer)
    {
        [thisTimer invalidate];
        thisTimer = nil;
    }
    
    //thisTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(asyncTest:) userInfo:nil repeats:YES];
}

- (void) sendData:(CDVInvokedUrlCommand*)command
{
    //NSLog(@"SCP - sendData to bluetooth");
    CDVPluginResult* pluginResult = nil;
    NSString* javaScript = nil;
    
    @try
    {
        NSString* characteristic = [command.arguments objectAtIndex:0];
        NSString* data = [command.arguments objectAtIndex:1];
        NSString* withResponse = [command.arguments objectAtIndex:2];
        
        if (data != nil)
        {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            javaScript = [pluginResult toSuccessCallbackString:command.callbackId];
            NSDictionary *dict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:characteristic, [data base64DecodedData], [NSNumber numberWithBool:[withResponse boolValue]], nil] forKeys:[NSArray arrayWithObjects:@"Characteristic", @"Data", @"WithResponse", nil]];
            
            [[NSNotificationCenter defaultCenter]  postNotificationName:@"QueueData" object:nil userInfo:dict];
        }
    } @catch (id exception)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_JSON_EXCEPTION messageAsString:[exception reason]];
        javaScript = [pluginResult toErrorCallbackString:command.callbackId];
    }
    
    [self writeJavascript:javaScript];
    
}

- (void) getIpAddress:(CDVInvokedUrlCommand*)command
{
    NSString *ipAddress = [self getIPAddress];
    NSLog(@"%@", ipAddress);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:ipAddress];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
